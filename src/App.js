import "./App.css";
import ProfileUser from "./Pages/ProfileUser";
import { Route, Routes } from "react-router-dom";

function App() {
  return (
    <Routes>
      <Route path="/" element={<ProfileUser />} />
      <Route path="/:userName" element={<ProfileUser />} />
    </Routes>
  );
}

export default App;
