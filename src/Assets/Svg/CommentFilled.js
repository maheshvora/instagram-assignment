import React from "react";

const CommentFilled = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      version="1.1"
    //   xmlns:xlink="http://www.w3.org/1999/xlink"
      width="512"
      height="512"
      x="0"
      y="0"
      viewBox="0 0 22 22"
    //   style="enable-background:new 0 0 512 512"
    //   xml:space="preserve"
    >
      <g>
        <path
          d="m21.08 15.4.91 5.43a1.022 1.022 0 0 1-.28.88A1.007 1.007 0 0 1 21 22a1.028 1.028 0 0 1-.17-.01l-5.43-.91A10.812 10.812 0 0 1 11 22a11 11 0 1 1 11-11 10.812 10.812 0 0 1-.92 4.4z"
          fill="#fff"
          opacity="1"
          data-original="#fff"
        ></path>
      </g>
    </svg>
  );
};

export default CommentFilled;
