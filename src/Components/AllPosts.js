import React from "react";
import ReelsFilled from "../Assets/Svg/ReelsFilled";
import MultiImage from "../Assets/Svg/MultiImage";
import LikeFilled from "../Assets/Svg/LikeFilled";
import CommentFilled from "../Assets/Svg/CommentFilled";
import { convertShortFormate } from "../Common/common";

const AllPosts = ({postItems}) => {
  return (
    <div className="all-posts">
      <ul className="all-posts-list">
        {postItems?.items?.length > 0 ? (
          postItems?.items?.map((item, i) => (
            <li key={i}>
              <div className="post-image">
                <span className="post-type">
                  {item.media_type === 2 ? <ReelsFilled /> : <MultiImage />}
                </span>
                <img src={item.display_url} alt="media" />
              </div>
              <div className="like-comment-wrap">
                <ul className="like-comment-list">
                  <li>
                    <span>
                      <LikeFilled />
                    </span>
                    <p>{convertShortFormate(item.like_count)}</p>
                  </li>
                  <li>
                    <span>
                      <CommentFilled />
                    </span>
                    <p>{convertShortFormate(item.comment_count)}</p>
                  </li>
                </ul>
              </div>
            </li>
          ))
        ) : (
          <p className="text-center">No Post Available</p>
        )}
      </ul>
    </div>
  );
};

export default AllPosts;
