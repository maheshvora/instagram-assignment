import React from "react";
import Link from "../Assets/Svg/Link";

const ProfileBio = ({ userDetails }) => {
  return (
    <>
      <div className="profile-bio">
        <p>{userDetails?.full_name}</p>
        <p className="user-profession">Personal blog</p>
        <p
          dangerouslySetInnerHTML={{
            __html: userDetails?.biography,
          }}
        />
      </div>
      <div className="profile-user-url">
        <span>
          <Link />
        </span>
        <a href="www.pommpoire.fr/253-collection-lingerie-camille-cerf">
          www.pommpoire.fr/253-collection-lingerie-camille-cerf
        </a>
      </div>
    </>
  );
};

export default ProfileBio;
