import React, { useState } from "react";
import "../Assets/Css/ProfileTab.css";
import Posts from "../Assets/Svg/Posts";
import Reels from "../Assets/Svg/Reels";
import Tagged from "../Assets/Svg/Tagged";
import AllPosts from "./AllPosts";
import ComingSoon from "./ComingSoon";

const ProfileTab = ({ postItems }) => {
  const [activeTab, setActiveTab] = useState("posts");
  const profileTab = [
    {
      key: "posts",
      icon: <Posts />,
      component: <AllPosts postItems={postItems} />,
    },
    {
      key: "reels",
      icon: <Reels />,
      component: <ComingSoon />,
    },
    {
      key: "tagged",
      icon: <Tagged />,
      component: <ComingSoon />,
    },
  ];

  const displayActiveTab = () => {
    return profileTab.map((item) => item.key === activeTab && item.component)
  }

  return (
    <div className="posts-tab-wrapper">
      <div className="posts-tab-menu">
        <ul>
          {profileTab?.map((item) => (
            <li key={item.key} className={activeTab === item.key ? "active" : ""}>
              <button onClick={() => setActiveTab(item.key)}>
                <span className="s-icon">
                  {item.icon}
                </span>
                <span className="s-text">{item.key}</span>
              </button>
            </li>
          ))}
        </ul>
      </div>
      {displayActiveTab()}
    </div>
  );
};

export default ProfileTab;
