import React from "react";

import "../Assets/Css/ProfileContent.css";
import Verified from "../Assets/Svg/Verified";
import ProfileInfo from "./ProfileInfo";
import ProfileBio from "./ProfileBio";

const ProfileContent = ({ userDetails }) => {

  return (
    <>
      <div className="profile-content-wrap">
        <div className="profile-image">
          <div>
            <span>
              <img
                src={userDetails?.profile_pic_url}
                alt="media"
              />
            </span>
          </div>
        </div>
        <div className="profile-details">
          <div className="profile-details-head">
            <div className="profile-username">
              <h2>{userDetails?.username}</h2>
              <span className="verified-icon">
                <Verified />
              </span>
            </div>
            <div className="follow-message-btn">
              <button type="button">Follow</button>
              <button type="button">Message</button>
            </div>
          </div>
          <div className="profile-info">
            <ProfileInfo userDetails={userDetails} />
          </div>
          <ProfileBio userDetails={userDetails} />
        </div>
      </div>
      <div className="profile-mobile-view">
        <div className="profile-content-wrap">
          <div className="profile-details">
            <ProfileBio userDetails={userDetails} />
            <div className="profile-info">
              <ProfileInfo userDetails={userDetails} />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ProfileContent;
