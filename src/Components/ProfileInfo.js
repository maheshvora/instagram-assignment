import React from "react";
import { convertShortFormate } from "../Common/common";

const ProfileInfo = ({userDetails}) => {
  return (
    <ul>
      <li>
        <button><span>{convertShortFormate(userDetails?.posts_count)} </span> <span>Posts</span></button>
      </li>
      <li>
        <button>
          <span>{convertShortFormate(userDetails?.follower_count)} </span> <span>followers</span>
        </button>
      </li>
      <li>
        <button>
          <span>{convertShortFormate(userDetails?.follow_count)} </span> <span>following</span>
        </button>
      </li>
    </ul>
  );
};

export default ProfileInfo;
