/**
 * @function numberFormat
 * @param {num} num Integer value that we need to convert
 * @returns Formatted number
 */
function numberFormat(num) {
  return new Intl.NumberFormat().format(Math.round(num * 10) / 10);
}

/**
 * @function convertShortFormate
 * @param {num} num Integer value that we want to convert in short form
 * @returns Short form number
 */

export const convertShortFormate = (num) => {
  if (num >= 1000000) return numberFormat(num / 1000000) + "M";
  if (num >= 9999) return numberFormat(num / 1000) + "K";
  return numberFormat(num);
}
