import { useEffect, useState } from "react";
import ProfileContent from "../Components/ProfileContent";
import ProfileTab from "../Components/ProfileTab";
import { useParams } from "react-router-dom";

const ProfileUser = () => {
  const [data, setData] = useState(null);
  const [loader, setLoader] = useState(false);
  const { userName } = useParams();
  console.log("userName", userName);
  const API_URL = `https://checkinsta.herokuapp.com/check_upk/${
    userName ? userName : "camillecerf"
  }?get_posts=true`;

  useEffect(() => {
    getProfileData();
  }, []);

  const getProfileData = () => {
    setLoader(true);
    fetch(API_URL)
      .then((response) => response.json())
      .then((json) => {
        setData(json);
        setLoader(false);
      })
      .catch((error) => {
        console.error(error);
        setLoader(false);
      });
  };

  return (
    <div className="profile-page-container">
      {!loader ? (
        data?.user && data?.posts ? (
          <>
            <ProfileContent userDetails={data?.user} />
            <ProfileTab postItems={data?.posts} />
          </>
        ) : (
          <p className="text-center">No Data Found</p>
        )
      ) : (
        <div className="loader">
          <a href="#">Loading</a>
        </div>
      )}
    </div>
  );
};

export default ProfileUser;
