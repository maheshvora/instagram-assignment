# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available scripts to run application in local machine

In the project directory, you need to install first all the required module using below command:

### `npm install`

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

## Run application in live mode

Open [https://instagram-assignment-maheshvora.vercel.app/](https://instagram-assignment-maheshvora.vercel.app/) to view it in your browser.

Above uil will show the default instagram profile of `camillecerf`

Now if you want open any profile dynamically from url like instagram then please append the username at the end of the above URL like bellow.

Open [https://instagram-assignment-maheshvora.vercel.app/facebook](https://instagram-assignment-maheshvora.vercel.app/facebook) to view it in your browser. Where `facebook` is the username we added end of the URL.
